# divvy-rspec-challenge

## OSX
### Setup
#### Install rbenv
```bash
brew install rbenv
```
#### Install ruby
```bash
rbenv install 2.5.3
```
#### Install bundler
```bash
gem install bundler
```
#### Install gems
```bash
gem install
```
#### Install drivers
* Note the script files are pinned to a certian version update as needed
* Chrome Browser needs to be installed
* Firefox Browser needs to be installed
* Run scripts to make webdrivers availible to Capybara
```bash
sudo ./setup/chromedriver.sh
sudo ./setup/geckodriver.sh
```

### Running Tests
#### Firefox
* Firefox modes
     - headless
     - real
* headless
```bash
DRIVER=headless_firefox BROWSER=firefox rspec
```
* real
```bash
DRIVER=selenium BROWSER=firefox rspec
```
#### Chrome
* Chrome modes
     - headless
     - real
* headless
```bash
DRIVER=headless_chrome BROWSER=chrome rspec
```
* real
```bash
DRIVER=selenium BROWSER=chrome rspec
```
#### Default (Firefox Headless)
* Run all tests
```bash
rspec
```
* tag options
    - ui
    - api
    - performance
```bash
rspec --tag <option>
```
* Results file will be saved as rspec_results.htm
* Screenshots will be saved on failures and cleared on the next run

## DOCKER
### Setup
* [docker setup instructions](https://docs.docker.com/)
* Build container
```bash
docker-compose build rspec
```
### Running Tests
#### Firefox
* Firefox modes
     - headless
* headless
```bash
docker-compose run -e DRIVER=headless_firefox rspec
```
#### Chrome
* Chrome modes
     - headless
* headless
```bash
docker-compose run -e DRIVER=headless_chrome rspec
```
#### Default (Firefox Headless)
* Run all tests
```bash
docker-compose run rspec
```
* tag options
    - ui
    - api
    - performance
```bash
docker-compose run -e TAG=<option> rspec
```
* To run with more ENV vars use `-e FOO -e BAR`
* Results file will be saved as rspec_results.htm
* Screenshots will be saved on failures and cleared on the next run
#### Cleanup
```bash
docker-compose down
```
