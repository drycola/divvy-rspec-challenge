#!/bin/bash
FIREFOX_VERSION="v0.23.0"
PACKAGE="unknown"

UNAMESTR=`uname`
if [[ "$UNAMESTR" == 'Linux' ]]; then
   PACKAGE="geckodriver-$FIREFOX_VERSION-linux64.tar.gz"
elif [[ "$UNAMESTR" == 'Darwin' ]]; then
   PACKAGE="geckodriver-$FIREFOX_VERSION-macos.tar.gz"
fi

if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
else
  rm /usr/local/share/$PACKAGE
  rm /usr/local/bin/geckodriver
  /usr/bin/geckodriver
  cd ~
  wget -O $PACKAGE https://github.com/mozilla/geckodriver/releases/download/$FIREFOX_VERSION/$PACKAGE
  mv $PACKAGE /usr/local/share/
  cd /usr/local/share/
  tar -zxvf $PACKAGE
  ln -sf /usr/local/share/geckodriver /usr/local/bin/geckodriver
  ln -sf /usr/local/share/geckodriver /usr/bin/geckodriver
  rm -fr $PACKAGE
fi
