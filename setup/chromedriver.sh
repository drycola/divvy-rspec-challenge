#!/bin/bash
CHROME_VERSION="2.43"
PACKAGE="unknown"

UNAMESTR=`uname`
if [[ "$UNAMESTR" == 'Linux' ]]; then
   PACKAGE='chromedriver_linux64.zip'
elif [[ "$UNAMESTR" == 'Darwin' ]]; then
   PACKAGE='chromedriver_mac64.zip'
fi

if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user to run chromedriver.sh" 2>&1
  exit 1
else
  rm /usr/local/share/$PACKAGE
  rm /usr/local/bin/chromedriver
  /usr/bin/chromedriver
  cd ~
  wget -O $PACKAGE https://chromedriver.storage.googleapis.com/$CHROME_VERSION/$PACKAGE
  mv $PACKAGE /usr/local/share/
  cd /usr/local/share/
  unzip -o $PACKAGE
  ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver
  ln -s /usr/local/share/chromedriver /usr/bin/chromedriver
  rm -fr $PACKAGE
fi

