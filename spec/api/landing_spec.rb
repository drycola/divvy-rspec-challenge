# frozen_string_literal: true

require 'requests'

base_url = 'https://www.facebook.com'
expected_header = ['text/html; charset="utf-8"'].freeze

describe 'facebook api', api: true do
  it "GET #{base_url} - Landing Page" do
    response = Requests.request('GET', base_url.to_s)

    expect(response.status).to be(200)
    expect(response.headers['content-type']).to eq(expected_header)
    expect(response.body).not_to be_empty
  end

  it "GET #{base_url}/consent/?dpr=2 - Signup Request" do
    response = Requests.request('GET', "#{base_url}/consent/?dpr=2")

    expect(response.status).to be(200)
    expect(response.headers['content-type']).to eq(expected_header)
    expect(response.body).not_to be_empty
  end
end
