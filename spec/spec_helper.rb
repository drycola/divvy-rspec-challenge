# frozen_string_literal: true

$LOAD_PATH << File.expand_path('..', __dir__)

require 'driver_helper'
require 'factory_bot'

ROOT_DIR = File.expand_path('./')

RSpec.configure do |config|
  ENV['TZ'] = 'UTC'
  config.color = ENV['COLOR'] == 'true'
  config.formatter = :documentation # :documentation, :progress, :html
  config.failure_color = :red
  config.tty = true
  config.fail_fast = ENV['FAIL_FAST'] == 'true'
  config.include Capybara::DSL
  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end
  config.after(:suite) do
    Capybara.reset_sessions!
  end
end
