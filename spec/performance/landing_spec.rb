# frozen_string_literal: true

require 'rspec-benchmark'

base_url = 'https://www.facebook.com'

describe 'facebook performance', performance: true do
  include RSpec::Benchmark::Matchers
  it "GET #{base_url} - Landing Page" do
    expect { Requests.request('GET', base_url.to_s) }.to perform_under(600).ms
  end

  it "GET #{base_url}/consent/?dpr=2 - Signup Request" do
    expect { Requests.request('GET', "#{base_url}/consent/?dpr=2") }.to perform_under(2500).ms
  end
end
