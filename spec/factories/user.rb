# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { 'John' }
    last_name  { 'Doe' }
    mobile { '435-586-6777' }
    invalid_mobile { '555-555-' }
    invalid_email { 'user@host' }
    invalid_password { '12' }
    email { 'user@host.com' }
    password { '1234567890ABCEDFG' }
  end
end
