# frozen_string_literal: true

FactoryBot.define do
  factory :facebook_selectors do
    page { 'https://www.facebook.com' } # css:
    first_name { 'firstname' } # :name
    last_name { 'lastname' } # :name
    warn_popup { '_5633 _5634 _53ij' } # :class
    mobile { 'reg_email__' } # :name
    email { 'reg_email__' } # :name
    email_confirmation { 'reg_email_confirmation__' } # :name
    password { 'reg_passwd__' } # :name
    password_warn { '#reg_error_inner' } # css:
    sign_up { 'websubmit' } # :name
    gender { '_58mt' } # :class
    warn_icons { 'uiStickyPlaceholderInput' } # class:
    gender_warn_icon { 'i._5k_6:nth-child(2)' } # class:
    birthday_warn_icon { 'i._5dbc:nth-child(3)' } # class:
    day_dropdown { '#day' } # css:
    month_dropdown { '#month' } # css:
    year_dropdown { '#year' } # css:
  end
end
