# frozen_string_literal: true

FactoryBot.define do
  factory :prompt do
    name_prompt { "What\u2019s your name?" }
    email_prompt  { 'You\'ll use this when you log in and if you ever need to reset your password.' }
    valid_email_prompt { 'Please enter a valid email address.' }
    reenter_email_prompt { 'Please re-enter your email address.' }
    mobile_prompt { 'Please enter a valid mobile number or email address.' }
    password_prompt { 'Enter a combination of at least six numbers, letters and punctuation marks (like ! and &).' }
    invalid_password_prompt { 'Your password must be at least 6 characters long. Please try another.' }
    gender_prompt { 'Please choose a gender. You can change who can see this later.' }
    birthday_prompt { 'Select your birthday. You can change who can see this later.' }
  end
end
