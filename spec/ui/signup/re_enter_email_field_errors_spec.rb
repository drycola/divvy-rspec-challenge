# frozen_string_literal: true

require 'page/facebook/reenter_email_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { ReEnterEmailHelper.new(selectors, page, user) }

  describe 'WHEN: Re-enter E-mail field is not populated but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_email
    end

    it 'THEN: Re-enter E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.reenter_email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Re-enter email] have warning icon.' do
      expected = ['', '', '', '', '', '', 'Re-enter email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Re-enter E-mail field is populated invalid but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.reenter_not_valid
    end

    it 'THEN: Re-enter E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.valid_email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Re-enter email] have warning icon.' do
      expected = ['', '', '', '', '', '', '']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Re-enter E-mail field is empty
  AND: Firstname field is populated.
  AND: Lastname field is populated.
  AND: E-mail field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.email_populated
    end

    it 'THEN: Re-enter E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.reenter_email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, New password, Re-enter email] have warning icons.' do
      expected = ['', '', '', '', 'Gender', 'New password', 'Re-enter email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Re-enter E-mail field is empty.
  AND: Firstname field is populated.
  AND: Lastname field is populated.
  AND: E-mail field is populated.
  AND: Password field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.password_populated
    end

    it 'THEN: Re-enter E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.reenter_email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, Re-enter email] have warning icons.' do
      expected = ['', '', '', '', '', 'Gender', 'Re-enter email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
