# frozen_string_literal: true

require 'page/facebook/mobile_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { MobileHelper.new(selectors, page, user) }

  describe 'WHEN: Mobile field is not valid.
  AND: All other fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.mobile_not_valid
    end

    it 'THEN: Mobile field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.mobile_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [] have warning icon.' do
      expected = ['', '', '', '', '', '']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
