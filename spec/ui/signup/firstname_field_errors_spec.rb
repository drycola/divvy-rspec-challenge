# frozen_string_literal: true

require 'page/facebook/firstname_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { FirstnameHelper.new(selectors, page, user) }

  describe 'WHEN: Firstname field is not populated but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_firstname
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [First name] have warning icon.' do
      expected = ['', '', '', '', '', 'First name']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Firstname field is empty.
  AND: Firstname field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.lastname_populated
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, First name, Mobile number or email] have warning icons.' do
      expected = ['', '', 'Gender', 'First name', 'Mobile number or email', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Firstname field is empty.
  AND: Firstname field is populated.
  AND: Mobile field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.mobile_populated
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, First name, New password] have warning icons.' do
      expected = ['', '', '', 'Gender', 'First name', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Firstname field is empty.
  AND: Firstname field is populated.
  AND: E-mail field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.email_populated
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, First name, New password, Re-enter email] have warning icons.' do
      expected = ['', '', '', 'Gender', 'First name', 'New password', 'Re-enter email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Firstname field is empty.
  AND: Firstname field is populated.
  AND: Email & re-enter email field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.reenter_email_populated
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, First name, New password] have warning icons.' do
      expected = ['', '', '', '', 'Gender', 'First name', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Firstname field is empty.
  AND: Firstname field is popluated.
  AND: Email & re-enter email field is populated.
  AND: Password field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.password_populated
    end

    it 'THEN: Firstname field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.name_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, First name] have warning icons.' do
      expected = ['', '', '', '', '', 'Gender', 'First name']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
