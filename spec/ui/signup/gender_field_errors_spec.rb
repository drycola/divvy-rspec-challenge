# frozen_string_literal: true

require 'page/facebook/gender_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { GenderHelper.new(selectors, page, user) }

  describe 'WHEN: Gender field is not selected but all other required fields populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_gender
    end

    it 'THEN: Gender prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.gender_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [] have warning icon.' do
      expected = ['', '', '', '', '', '', '']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
