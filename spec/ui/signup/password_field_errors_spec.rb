# frozen_string_literal: true

require 'page/facebook/password_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { PasswordHelper.new(selectors, page, user) }

  describe 'WHEN: Password field is not populated but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_password
    end

    it 'THEN: Password field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.password_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [New password] have warning icon.' do
      expected = ['', '', '', '', '', '', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Password field is populated invalid but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.password_not_valid
    end

    it 'THEN: Password field prompt is displayed.' do
      actual = facebook.password_warn.text
      expected = prompt.invalid_password_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [New password] have warning icon.' do
      expected = ['', '', '', '', '', '', '']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Password field is empty
  AND: Firstname field is populated.
  AND: Lastname field is populated.
  AND: E-mail field is populated.
  AND: Re-enter E-mail field is populated
  AND: Signup button is clicked.' do
    before :each do
      facebook.reenter_email_populated
    end

    it 'THEN: Password field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.password_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, New password, Re-enter email] have warning icons.' do
      expected = ['', '', '', '', '', 'Gender', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
