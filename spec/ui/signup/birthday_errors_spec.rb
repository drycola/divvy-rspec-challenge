# frozen_string_literal: true

require 'page/facebook/birthday_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { BirthdayHelper.new(selectors, page, user) }

  describe 'WHEN: Invalid birthday is selected.
  AND: Signup button is clicked.' do
    before :each do
      facebook.bad_birthday
    end

    it 'THEN: Fields [Mobile number or email] have warning icon.' do
      expected = ['', 'Birthday', 'First name', 'Last name', 'Mobile number or email', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Invalid Day birthday is selected.
  AND: Firstname is populated.
  AND: Lastname is populated.
  AND: Mobile is populated.
  AND: Password is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.bad_day_missing_gender
    end

    it 'THEN: Birthday field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.birthday_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Mobile number or email] have warning icon.' do
      expected = ['', '', '', '', '', '', 'Gender']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Invalid Month birthday is selected.
  AND: Firstname is populated.
  AND: Lastname is populated.
  AND: Mobile is populated.
  AND: Password is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.bad_month_missing_gender
    end

    it 'THEN: Birthday field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.birthday_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Mobile number or email] have warning icon.' do
      expected = ['', '', '', '', '', '', 'Gender']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: Invalid Year birthday is selected.
  AND: Firstname is populated.
  AND: Lastname is populated.
  AND: Mobile is populated.
  AND: Password is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.bad_year_missing_gender
    end

    it 'THEN: Birthday field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.birthday_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Mobile number or email] have warning icon.' do
      expected = ['', '', '', '', '', '', 'Gender']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
