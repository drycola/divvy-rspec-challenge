# frozen_string_literal: true

require 'page/facebook/email_helper.rb'
require 'lib/user.rb'
require 'lib/prompt.rb'

describe 'GIVEN: https://www.facebook.com', ui: true do
  let(:user) { build(:user) }
  let(:prompt) { build(:prompt) }
  let(:selectors) { build(:facebook_selectors) }
  let(:facebook) { EmailHelper.new(selectors, page, user) }

  describe 'WHEN: E-mail field is not populated but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_email
    end

    it 'THEN: E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Mobile number or email] have warning icon.' do
      expected = ['', '', '', '', '', 'Mobile number or email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: E-mail field is populated invald but all other required fields are.
  AND: Signup button is clicked.' do
    before :each do
      facebook.bad_email
    end

    it 'THEN: E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.mobile_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [] have warning icon.' do
      expected = ['', '', '', '', '', '']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: E-mail field is empty
  AND: Firstname field is populated.
  AND: Lastname field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_email_password_gender
    end

    it 'THEN: E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, Mobile number or email] have warning icons.' do
      expected = ['', '', '', 'Gender', 'Mobile number or email', 'New password']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end

  describe 'WHEN: E-mail field is empty.
  AND: Firstname field is populated.
  AND: Lastname field is populated.
  AND: Password field is populated.
  AND: Signup button is clicked.' do
    before :each do
      facebook.missing_email_gender
    end

    it 'THEN: E-mail field prompt is displayed.' do
      actual = facebook.warn_popup.text
      expected = prompt.email_prompt
      expect(actual).to eq(expected)
    end

    it 'THEN: Fields [Gender, Mobile number or email] have warning icons.' do
      expected = ['', '', '', '', 'Gender', 'Mobile number or email']
      actual = facebook.warn_icons_array
      expect(actual).to match_array(expected)
    end
  end
end
