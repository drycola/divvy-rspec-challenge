# frozen_string_literal: true

require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec'
require 'capybara-screenshot/rspec'
require 'selenium-webdriver'

@browser ||= ENV['BROWSER'] ? ENV['BROWSER'].to_sym : :firefox
@driver ||= ENV['DRIVER'] ? ENV['DRIVER'].to_sym : :headless_firefox

Capybara.configure do |config|
  config.automatic_reload = true
  config.run_server = false
  config.default_selector = :css
  config.default_driver = @driver
  config.javascript_driver = @driver
  config.default_max_wait_time = 0.01
  config.match = :prefer_exact
  config.ignore_hidden_elements = true
  config.threadsafe = true
end

# Keep only the screenshots generated from the last failing test suite
Capybara::Screenshot.prune_strategy = :keep_last_run
Capybara::Screenshot.autosave_on_failure = true

Capybara.register_driver :headless_chrome do |app|
  browser_options = Selenium::WebDriver::Chrome::Options.new
  browser_options.args << '--headless'
  browser_options.args << '--disable-gpu'
  browser_options.args << '--no-sandbox'
  Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
end

Capybara.register_driver :headless_firefox do |app|
  browser_options = Selenium::WebDriver::Firefox::Options.new
  browser_options.args << '--headless'
  browser_options.args << '--disable-gpu'
  browser_options.args << '--no-sandbox'
  Capybara::Selenium::Driver.new(app, browser: :firefox, options: browser_options)
end

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: @browser)
end

Capybara.add_selector(:id) do
  xpath { |id| XPath.descendant[XPath.attr(:id) == id.to_s] }
end

Capybara.add_selector(:name) do
  xpath { |name| XPath.descendant[XPath.attr(:name) == name.to_s] }
end

Capybara.add_selector(:style) do
  xpath { |style| XPath.descendant[XPath.attr(:style) == style.to_s] }
end

Capybara.add_selector(:class) do
  xpath { |klass| XPath.descendant[XPath.attr(:class) == klass.to_s] }
end

Capybara.add_selector(:tag_name) do
  xpath { |tag_name| XPath.descendant[XPath.attr(:tag_name) == tag_name.to_s] }
end

Capybara.add_selector(:type) do
  xpath { |type| XPath.descendant[XPath.attr(:type) == type.to_s] }
end

Capybara.add_selector(:row) do
  xpath { |num| ".//tbody/tr[#{num}]" }
end
