# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# PasswordHelper contains step implementation for password
class PasswordHelper < Facebook
  def missing_password
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    gender('male').click
    sign_up.click
  end

  def password_not_valid
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.invalid_password)
    gender('male').click
    sign_up.click
    birthday_submit('Yes')
  end

  def reenter_email_populated
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    sign_up.click
  end
end
