# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# ReEnterEmailHelper contains step implementation for re-enter-email
class ReEnterEmailHelper < Facebook
  def missing_email
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def reenter_not_valid
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.invalid_email)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def email_populated
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    sign_up.click
  end

  def password_populated
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    password.set(@user.password)
    sign_up.click
  end
end
