# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# BirthdayHelper contains step implementation for birthday
class BirthdayHelper < Facebook
  def bad_birthday
    visit
    day('Day')
    month('Month')
    year('Year')
  end

  def bad_day_missing_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    day('Day')
    sign_up.click
  end

  def bad_month_missing_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    month('Month')
    sign_up.click
  end

  def bad_year_missing_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    year('Year')
    sign_up.click
  end
end
