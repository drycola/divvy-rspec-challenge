# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# MobileHelper contains step implementation for mobile
class MobileHelper < Facebook
  def mobile_not_valid
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    mobile.set(@user.invalid_mobile)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end
end
