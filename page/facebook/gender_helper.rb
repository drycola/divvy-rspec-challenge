# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# GenderHelpr contains step implementation for gender
class GenderHelper < Facebook
  def missing_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    sign_up.click
  end
end
