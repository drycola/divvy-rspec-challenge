# frozen_string_literal: true

require 'lib/facebook_selectors.rb'

# Page objects and methods for facebook main page
class Facebook
  def initialize(selectors, driver, user)
    @selectors = selectors
    @driver = driver
    @user = user
  end

  def visit
    @driver.visit @selectors.page
  end

  def first_name
    @driver.find(:name, @selectors.first_name)
  end

  def last_name
    @driver.find(:name, @selectors.last_name)
  end

  def warn_popup
    @driver.find(:class, @selectors.warn_popup, wait: 0.1)
  end

  def mobile
    @driver.find(:name, @selectors.mobile)
  end

  def email
    @driver.find(:name, @selectors.email)
  end

  def email_confirmation
    @driver.find(:name, @selectors.email_confirmation)
  end

  def password
    @driver.find(:name, @selectors.password)
  end

  def password_warn
    @driver.find(@selectors.password_warn, wait: 2)
  end

  def sign_up
    @driver.find(:name, @selectors.sign_up)
  end

  def gender(gender)
    # use find first because all can happen faster then the page will load
    @driver.find(:class, @selectors.gender)
    options = @driver.all(:class, @selectors.gender)
    return options[0] if gender == 'female'
    return options[1] if gender == 'male'
  end

  def warn_icons
    # use find first because all can happen faster then the page will load
    @driver.find(class: @selectors.warn_icons)
    @driver.all(class: @selectors.warn_icons)
  end

  def gender_warn_icons
    begin
      @driver.find(@selectors.gender_warn_icon, wait: 0.1)
    rescue StandardError
      # do nothing it is okay
    end
    if @driver.has_css?(@selectors.gender_warn_icon, visible: true)
      return 'Gender'
    else
      return ''
    end
  end

  def warn_icons_array
    fields = []
    warn_icons.each do |elements|
      fields.push(elements.text)
    end
    fields.push(gender_warn_icons)
    fields.push(birthday_warn_icon)
    fields
  end

  def birthday_submit(button_string)
    @driver.click_on(button_string)
  end

  def birthday_warn_icon
    begin
      @driver.find(@selectors.birthday_warn_icon, wait: 0.1)
    rescue StandardError
      # do nothing it is okay
    end
    if @driver.has_css?(@selectors.birthday_warn_icon, visible: true)
      return 'Birthday'
    else
      return ''
    end
  end

  def day(day_string)
    @driver.find(@selectors.day_dropdown).find('option', text: day_string).select_option
  end

  def month(month_string)
    @driver.find(@selectors.month_dropdown).find('option', text: month_string).select_option
  end

  def year(year_string)
    @driver.find(@selectors.year_dropdown).find('option', text: year_string).select_option
  end
end
