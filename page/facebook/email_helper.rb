# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# EmailHelper contains step implementation for email
class EmailHelper < Facebook
  def missing_email
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def missing_email_password_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    sign_up.click
  end

  def bad_email
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    email.set(@user.invalid_email)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def missing_email_gender
    visit
    first_name.set(@user.first_name)
    last_name.set(@user.last_name)
    password.set(@user.password)
    sign_up.click
  end
end
