# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# FirstnameHelper contains step implementation for firstname
class FirstnameHelper < Facebook
  def missing_firstname
    visit
    last_name.set(@user.last_name)
    email.set(@user.mobile)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def lastname_populated
    visit
    last_name.set(@user.last_name)
    sign_up.click
  end

  def mobile_populated
    visit
    last_name.set(@user.last_name)
    mobile.set(@user.mobile)
    sign_up.click
  end

  def email_populated
    visit
    last_name.set(@user.last_name)
    email.set(@user.email)
    sign_up.click
  end

  def reenter_email_populated
    visit
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    sign_up.click
  end

  def password_populated
    visit
    last_name.set(@user.last_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    sign_up.click
  end
end
