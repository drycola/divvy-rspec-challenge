# frozen_string_literal: true

require 'page/facebook/facebook.rb'

# LastnameHelper contains step implementation for lastname
class LastnameHelper < Facebook
  def missing_lastname
    visit
    first_name.set(@user.first_name)
    email.set(@user.mobile)
    password.set(@user.password)
    gender('male').click
    sign_up.click
  end

  def firstname_populated
    visit
    first_name.set(@user.first_name)
    sign_up.click
  end

  def mobile_populated
    visit
    first_name.set(@user.first_name)
    mobile.set(@user.mobile)
    sign_up.click
  end

  def email_populated
    visit
    first_name.set(@user.first_name)
    email.set(@user.email)
    sign_up.click
  end

  def reenter_email_populated
    visit
    first_name.set(@user.first_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    sign_up.click
  end

  def password_populated
    visit
    first_name.set(@user.first_name)
    email.set(@user.email)
    email_confirmation.set(@user.email)
    password.set(@user.password)
    sign_up.click
  end
end
