#!/usr/bin/env bash

./setup/chromedriver.sh
./setup/geckodriver.sh

bundle install

if [ -n "$TAG" ]; then
    rspec --tag $TAG
else
    rspec
fi
