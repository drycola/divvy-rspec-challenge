# frozen_string_literal: true

# User contains user field data
class Prompt
  attr_accessor :name_prompt
  attr_accessor :email_prompt
  attr_accessor :valid_email_prompt
  attr_accessor :reenter_email_prompt
  attr_accessor :mobile_prompt
  attr_accessor :password_prompt
  attr_accessor :invalid_password_prompt
  attr_accessor :gender_prompt
  attr_accessor :birthday_prompt
end
