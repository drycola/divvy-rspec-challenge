# frozen_string_literal: true

# User contains user field data
class FacebookSelectors
  attr_accessor :page
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :warn_popup
  attr_accessor :mobile
  attr_accessor :email
  attr_accessor :email_confirmation
  attr_accessor :password
  attr_accessor :password_warn
  attr_accessor :sign_up
  attr_accessor :gender
  attr_accessor :warn_icons
  attr_accessor :gender_warn_icon
  attr_accessor :birthday_warn_icon
  attr_accessor :day_dropdown
  attr_accessor :month_dropdown
  attr_accessor :year_dropdown
end
