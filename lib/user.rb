# frozen_string_literal: true

# User contains user field data
class User
  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :mobile
  attr_accessor :invalid_mobile
  attr_accessor :invalid_email
  attr_accessor :invalid_password
  attr_accessor :email
  attr_accessor :password
end
