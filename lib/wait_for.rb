module WaitFor
  require 'timeout'
  module Waiter
    # Waits until block yields true or timeout expires
    def wait_for(timeout: 10, interval: 0.1, debug: :none, message: 'wait_for', &block)
      puts "\nwait_for called by #{caller_locations(1, 1)[0].label} with a wait time of #{timeout}" if debug == :debug
      start_time = Time.now
      return Timeout.timeout(timeout) do
        loop do
          print "#{message}: #{Time.now - start_time}   \r"
          $stdout.flush
          result = yield result, self if block_given?
          if result
            print "                                                       \r"
            $stdout.flush
            break result
          end
          sleep interval
        end
      end
    rescue Timeout::Error => e
      puts "The block never evaluated to true within #{timeout} second#{'s' if timeout > 1}" unless debug == :none
      output = []
      e.backtrace.map do |line|
        out = line.split(':')
        formatted_string = "File:    #{out[0]}\n  Line:    #{out[1]}\n  Method: #{out[2].gsub!('in ', '').strip}"
        output.push(formatted_string)
        break output if formatted_string =~ %r{spec\/.*.rb}
      end
      puts output.join("\n") if debug == :full
      puts output[-1] if debug == :info
      return false
    ensure
      time_waited = Time.now - start_time
      puts "wait_for took #{time_waited} second#{'s' unless time_waited == 1}\n" if debug == :debug
    end
  end
end
